/**
 * View Models used by Spring MVC REST controllers.
 */
package com.magar.ooii.web.rest.vm;
